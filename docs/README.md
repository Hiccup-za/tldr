---
home: true
heroImage: images/logo.png
heroText: 
tagline: 
actionText: Get Started →
actionLink: /games/
footer: COPYRIGHT © Christopher Zeuch 2021 - present
---

---

### Ratings

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Indifferent" type="indifferent"/>
<colorBadge text="Not recommended" type="notRecommended"/>

### Platforms

<colorBadge text="Playstation 5" type="ps5"/>
<colorBadge text="Playstation 4" type="ps4"/>
<colorBadge text="Xbox One" type="xboxOne"/>
<colorBadge text="Nintendo Switch" type="switch"/>

### Subscription Services

<colorBadge text="PS Plus" type="psPlus"/>
<colorBadge text="Xbox Games Pass" type="xboxGamesPass"/>

<br></br>
