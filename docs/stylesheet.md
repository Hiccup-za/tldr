---
sidebar: auto
---

# Stylesheet

## Badges

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Indifferent" type="indifferent"/>
<colorBadge text="Not recommended" type="notRecommended"/>
<colorBadge text="Playstation 5" type="ps5"/>
<colorBadge text="Playstation 4" type="ps4"/>
<colorBadge text="Xbox One" type="xboxOne"/>
<colorBadge text="Nintendo Switch" type="switch"/>
<colorBadge text="PS Plus" type="psPlus"/>
<colorBadge text="Xbox Games Pass" type="xboxGamesPass"/>

```md
<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Indifferent" type="indifferent"/>
<colorBadge text="Not recommended" type="notRecommended"/>
<colorBadge text="Playstation 5" type="ps5"/>
<colorBadge text="Playstation 4" type="ps4"/>
<colorBadge text="Xbox One" type="xboxOne"/>
<colorBadge text="Nintendo Switch" type="switch"/>
<colorBadge text="PS Plus" type="psPlus"/>
<colorBadge text="Xbox Games Pass" type="xboxGamesPass"/>
```

## Containers

::: tip Tip Heading
Text goes here.
:::

```md
::: tip Tip Heading
Text goes here.
:::
```

::: warning Warning Heading
Text goes here.
:::

```md
::: warning Warning Heading
Text goes here.
:::
```

::: danger Danger Heading
Text goes here.
:::

```md
::: danger Danger Heading
Text goes here.
:::
```
