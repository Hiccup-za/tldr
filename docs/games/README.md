# 2021

## Alan Wake Remastered

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Playstation 5" type="ps5"/>

![image](./covers/2021/alan-wake-remastered.jpg)

## Diablo II Resurrected

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Playstation 5" type="ps5"/>

![image](./covers/2021/diablo-2-resurrected.jpg)

## Borderlands 3

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Playstation 5" type="ps5"/>

![image](./covers/2021/borderlands-3-ultimate-edition.jpg)

## Demon Souls

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Playstation 5" type="ps5"/>

![image](./covers/2021/demon-souls.jpg)

## Psychonauts 2

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Xbox One" type="xboxOne"/>
<colorBadge text="Xbox Games Pass" type="xboxGamesPass"/>

![image](./covers/2021/psychonauts-2.jpg)

## Myst

<colorBadge text="Indifferent" type="indifferent"/>
<colorBadge text="Xbox One" type="xboxOne"/>
<colorBadge text="Xbox Games Pass" type="xboxGamesPass"/>

![image](./covers/2021/myst.jpg)

## The Ascent

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Xbox One" type="xboxOne"/>
<colorBadge text="Xbox Games Pass" type="xboxGamesPass"/>

![image](./covers/2021/the-ascent.jpg)

## Dead Space

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Xbox One" type="xboxOne"/>
<colorBadge text="Xbox Games Pass" type="xboxGamesPass"/>

![image](./covers/2021/dead-space.jpg)

## Resident Evil Village

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2021/resident-evil-village.jpg)

## Torchlight 3

<colorBadge text="Indifferent" type="indifferent"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2021/torchlight-3.jpg)

## Enter the Gungeon

<colorBadge text="Indifferent" type="indifferent"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2021/enter-the-gungeon.jpg)

## Grim Fandango Remastered

<colorBadge text="Indifferent" type="indifferent"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2021/grim-fandango.jpg)

## The Legend of Zelda: Links Awakening

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Nintendo Switch" type="switch"/>

![image](./covers/2021/links-awakening.jpg)

## Greedfall

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2021/greedfall.jpg)

## Nioh 2

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2021/nioh2.jpg)

## Assassins Creed Odyssey

<colorBadge text="Indifferent" type="indifferent"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2021/assassins-creed-odyssey.jpg)

## Mario + Rabbids Kingdom Battle

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Nintendo Switch" type="switch"/>

![image](./covers/2021/mario-rabbids.jpg)
