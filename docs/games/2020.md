# 2020

## Mortal Shell

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2020/mortal-shell.jpg)

## Dead Rising

<colorBadge text="Indifferent" type="indifferent"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2020/dead-rising.jpg)

## Hades

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Nintendo Switch" type="switch"/>

![image](./covers/2020/hades.jpg)

## Genshin Impact

<colorBadge text="Indifferent" type="indifferent"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2020/genshin-impact.jpg)

## Super Mario 3D All Stars

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Nintendo Switch" type="switch"/>

![image](./covers/2020/super-mario-3d-all-stars.jpg)

## Tony Hawk's Pro Skater 1 + 2

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2020/tony-hawks.jpg)

## Destroy All Humans

<colorBadge text="Indifferent" type="indifferent"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2020/destroy-all-humans.jpg)

## Spyro Reignited Trilogy

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2020/spyro.jpg)

## Fall Guys: Ultimate Knockout

<colorBadge text="Indifferent" type="indifferent"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2020/fall-guys.jpg)

## Tekken 7

<colorBadge text="Indifferent" type="indifferent"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2020/tekken-7.jpg)

## Anthem

<colorBadge text="Indifferent" type="indifferent"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2020/anthem.jpg)

## Far Cry 5

<colorBadge text="Indifferent" type="indifferent"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2020/far-cry-5.jpg)

## Need For Speed Heat

<colorBadge text="Indifferent" type="indifferent"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2020/nfs-heat.jpg)

## Battlefield V

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2020/battlefield-5.jpg)

## NHL 20

<colorBadge text="Indifferent" type="indifferent"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2020/nhl-20.jpg)

## Animal Crossing: New Horizons

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Nintendo Switch" type="switch"/>

![image](./covers/2020/animal-crossing-new-horizons.jpg)

## Marvel's Spider-Man

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2020/spider-man.jpg)

## Tom Clancy's The Division 2

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Playstation 4" type="ps4"/>

![image](./covers/2020/tom-clancy-division-2.jpg)

## Resident Evil 2

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Playstation 4" type="ps4"/>

:page_with_curl: [Review](/gaming-reviews/2020/resident-evil-2/)

![image](./covers/2020/resident-evil-2.jpg)
