# 2016

## Rise of the Tomb Raider

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Xbox One" type="xboxOne"/>

![image](./covers/2016/rise-of-the-tomb-raider.jpg)

## Doom

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Xbox One" type="xboxOne"/>

![image](./covers/2016/doom.jpg)

## Lords of the Fallen

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Xbox One" type="xboxOne"/>

![image](./covers/2015/../2016/lords-of-the-fallen.jpg)

## Sherlock Holmes: Crimes and Punishments

<colorBadge text="Recommended" type="recommended"/>
<colorBadge text="Xbox One" type="xboxOne"/>

![image](./covers/2016/sherlock-holmes.jpg)
