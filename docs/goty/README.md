---
sidebar: auto
---

# Game of the Year

## 2021

### Contenders

- Demon Souls
- Psychonaughts 2
- Resident Evil Village

## 2020

![image](../games/covers/2020/animal-crossing-new-horizons.jpg)

### Contenders

![image](./covers/2020.jpg)

## 2019

![image](../games/covers/2019/cod-modern-warfare.jpg)

### Contenders

![image](./covers/2019.jpg)

## 2018

![image](../games/covers/2018/broken-age.jpg)

### Contenders

![image](./covers/2018.jpg)

## 2017

![image](../games/covers/2017/resident-evil-7.jpg)

### Contenders

![image](./covers/2017.jpg)
